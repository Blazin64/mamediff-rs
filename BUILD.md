# MAMEDiff (Rust Edition) Build Instructions

## Requirements
* The ability to use a command-line terminal.
* Rust (1.62.1).
* The Git client.

## Build Process

Run the following commands in a terminal.

1. Download the repository

```sh
git clone https://gitlab.com/blazin64/mamediff-rs.git
```

2. Switch to the project directory

```sh
cd mamediff-rs
```

3. Build with Cargo (and automatically download dependencies)

```sh
cargo build --release
```

If the build is successful, your new binary will be under `target/release`. It should be named `mamediff`.
