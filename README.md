# MAMEDiff (Rust Edition)
![License GPLv3](https://shields.io/badge/License-GPL%20v3-blue?logo=opensourceinitiative&style=for-the-badge) ![Built With Rust](https://shields.io/badge/Built%20With-Rust-brown?logo=rust&style=for-the-badge)

A CLI utility that helps create [MAME](https://www.mamedev.org/) update & rollback XML datfiles. It will eventually provide much of the
functionality of Logiqx's (seemingly abandoned) [MAMEDiff](http://www.logiqx.com/Tools/).

Update datfiles can help determine exactly which files are required for compatibility with a newer version of MAME.
Rollback datfiles do the same thing, but for downgrading MAME versions instead. Combined with a ROM manager utility,
the resulting update or rollback datfiles can help verify that newly added files are correct.

Some relevant software is listed under [Resources](#resources).

## Development Status:

| Feature                            | Status      |
|------------------------------------|-------------|
| Read & write datfiles              | Done        |
| Comparison reports                 | In progress |
| Logiqx's MAMEDiff functions        | TBD         |
| Type Filters (CHDs, ROMs, Samples) | TBD         |

## Usage

__Notes__
* This information may be outdated. For the latest usage information, you will need to build this project from source. 
See the installation section for how to do this.

```
A utility for comparing MAME datfiles.

Usage: mamediff [OPTIONS] (-p OR -x <output file>) <base file> <target file>

Arguments:
  <base file>    Name of the base XML file
  <target file>  Name of the target XML file

Options:
  -x, --xml <output file>  Name of the output XML file
  -p, --print              Show basic stats about the differences (increase detail with -v)
  -v, --verbose            Show extra details during operation
  -h, --help               Print help
  -V, --version            Print version
```

## Installation

For now, there are no binary releases. As a result, you will need to [compile the source code](BUILD.md).

## File Formats

__Supported MAME Datfile Formats:__
* ListXML
  * Used by MAME versions >= 0.162 (May 27, 2015)
    * Support down to 0.68 may be possible in the future. 

__Unsupported MAME Datfile Formats:__
* ListInfo
  * Used by MAME versions < 0.84 (July 2, 2004)
* ListXML (without SHA1)
  * Used by MAME versions < 0.68 (May 16, 2003)

## FAQs

__Why Rust?__
* Rust has amazing dependency management via Cargo, and it is somewhat safer than C/C++.
* My preferred language, Python, is not optimal for this type of task.
* C/C++ are not ideal when it comes to dependency management.

__Why did you make this?__
* There aren't any actively maintained cross-platform tools that can quickly show the difference between datfiles.
* The alternative approach, creating fix datfiles from ROM set scans, is extremely slow (and fundamentally problematic).

__Why are only XML datfiles supported?__
* Listinfo is a legacy format. Support may be considered in the future.

## Resources:

__Similar Tools__

| Name                                                                   | Windows Support | Linux Support | MacOS Support |
|------------------------------------------------------------------------|-----------------|---------------|---------------|
| [MAMEDiff](https://github.com/Logiqx/logiqx-dev/tree/master/MAMEDiff/) | Yes             | Yes, via WINE | Yes, via WINE |
| [SabreTools](https://github.com/SabreTools/SabreTools)                 | Yes             | No\*          | No            |

\* It is technically possible to compile a Linux binary for SabreTools. However, most features are broken and have
unexpected errors. Generating update/rollback DATs __seems__ to work, but may also have issues.

__ROM Managers__

| Name                                                 | Windows Support | Linux Support | MacOS Support |
|------------------------------------------------------|-----------------|---------------|---------------|
| [Clrmamepro](https://mamedev.emulab.it/clrmamepro/)  | Yes             | Yes, via WINE | Yes, via WINE |
| [JRomManager](https://github.com/optyfr/JRomManager) | Yes             | Yes           | Yes           |
| [ROMba](https://github.com/uwedeportivo/romba/)      | No              | Yes           | Yes           |
| [Romorganizer](https://github.com/xprism1/romog)     | No              | Yes           | No            |
| [Romulus](https://github.com/ArthurMoore85/romulus)  | No              | Yes           | No            |
| [ROMVault](https://romvault.com/)                    | Yes             | Yes, via Mono | Yes, via Mono |
