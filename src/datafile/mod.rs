/*
    MAMEDiff (Rust Edition) - A utility & library for comparing MAME datfiles
    Copyright (C) 2021-2024  Thomas Goodman

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use serde::{Deserialize, Serialize};

pub use super::machine::*;

pub mod datafile_impl;
#[derive(Debug, Serialize, Deserialize, PartialEq)]
#[serde(rename = "datafile")]
pub struct Datafile {
    #[serde(rename = "header")]
    pub header: Option<Header>,
    #[serde(rename = "machine", default)]
    pub machines: Vec<Machine>,
}

#[derive(Debug, Serialize, Deserialize, PartialEq)]
pub struct Header {
    pub name: String,
    pub description: String,
    pub version: String,
    pub author: String,
}

pub struct Changes {
    pub unchanged: Vec<String>,
    pub modified: Vec<String>,
    pub deleted: Vec<String>,
    pub added: Vec<String>,
    pub renamed: Vec<(String, String)>
}