/*
    MAMEDiff (Rust Edition) - A utility & library for comparing MAME datfiles
    Copyright (C) 2021-2024  Thomas Goodman

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use std::collections::{HashMap, HashSet};

pub use super::{Datafile,Header,Machine,Changes};

impl Datafile {
    /// Generate hashes for each machine entry. This is needed for the comparison process.
    pub fn hash_machines(&mut self) {
        for machine in &mut self.machines {
            machine.hash_all();
        }
    }

    /// Compare this Datafile instance (A) to another one (B). The comparison direction is A -> B.
    /// If the reverse is needed, this function can be run from B instead of A.
    /// Takes a reference to the Datafile to be compared with.
    pub fn compare(&self, new_data: &Datafile, enable_verbose: &bool) -> Changes {
        let verbose = *enable_verbose;
        let mut old_map: HashMap<String, &Machine> = HashMap::new();
        let mut new_map: HashMap<String, &Machine> = HashMap::new();

        // Populate old_map, only retaining machines with hashes.
        // As a bonus, this allows the safe use of unwrap() on hash fields.
        for machine in &self.machines {
            if machine.md_main_hash.is_some() {
                old_map.insert(machine.name.clone(), &machine);
            }
        }

        // Populate new_map, only retaining machines with hashes.
        for machine in &new_data.machines {
            if machine.md_main_hash.is_some() {
                new_map.insert(machine.name.clone(), &machine);
            }
        }

        if verbose {
            println!("Valid old machines: {}", old_map.len());
            println!("Valid new machines: {}\n", new_map.len());
        }

        // Storage for comparison results
        let mut results = Changes {
            unchanged: Vec::new(),
            modified: Vec::new(),
            deleted: Vec::new(),
            added: Vec::new(),
            renamed: Vec::new()
        };

        // Maps from hashes to machine names, used for comparisons.
        let mut hash_name: HashMap<[u8; 32], String> = HashMap::new();
        let mut hash_name_new: HashMap<[u8; 32], String> = HashMap::new();

        // Populate the hash_name map.
        for (name, &machine) in &old_map {
            hash_name.insert(machine.md_main_hash.unwrap(), name.clone());
        }

        // Populate the hash_name_new map.
        for (name, &machine) in &new_map {
            hash_name_new.insert(machine.md_main_hash.unwrap(), name.clone());
        }

        // Identify unchanged, modified, renamed, and deleted machines.
        for (name, &machine) in &old_map {
            // If a machine with the same name exists in new_map, it is unchanged or modified.
            // There is also a chance it was renamed.
            if let Some(&new_machine) = new_map.get(name) {
                // A machine is unchanged if it has the same name and hash.
                if machine.md_main_hash == new_machine.md_main_hash {
                    results.unchanged.push(name.clone());
                } else {
                    // A machine is renamed if it has a new name, but the same hash.
                    if let Some(name_new) = hash_name_new.get(&machine.md_main_hash.unwrap()) {
                        results.renamed.push((name.clone(), name_new.clone()));
                    } else {
                        // A machine is modified if it has the same name, but a different hash.
                        results.modified.push(name.clone());
                    }
                }
            } else {
                // If a machine is renamed if it has a new name, but the same hash.
                if let Some(name_new) = hash_name_new.get(&machine.md_main_hash.unwrap()) {
                    results.renamed.push((name.clone(), name_new.clone()));
                } else {
                    // A machine is deleted if no machine with the same name or hash exists.
                    results.deleted.push(name.clone());
                }
            }
        }

        // Identify added machines and fix possible incorrect classifications.
        for (name_new, &machine_new) in &new_map {
            // A machine was probably added if the name is not in old_map.
            if !old_map.contains_key(name_new) {
                // If an old machine with the given hash is not present, it was added.
                if !hash_name.contains_key(&machine_new.md_main_hash.unwrap()) {
                    results.added.push(name_new.clone());
                }
            } else {
                // Handle some special cases
                for (first, second) in &results.renamed {
                    // A machine was renamed, but a new machine with the old name was added.
                    if first == name_new {
                        if !hash_name.contains_key(&machine_new.md_main_hash.unwrap()) {
                            results.added.push(name_new.clone());
                        }
                    }
                    // A machine was renamed when a machine with the new name already existed.
                    if second == name_new && results.modified.contains(name_new) {
                        results.deleted.push(name_new.clone());
                        // The machine was likely incorrectly classified as modified. It can be
                        // safely removed from the vector of modified machines
                        results.modified.retain(|x| x != name_new);
                    }
                }
            }
        }

        results
    }

    pub fn diff_datafile(&self, changes: &Changes) -> Datafile {
        let header: Header = Header {
            name : "MAMEDiff Results".to_string(),
            description : "MAME Update/Rollback datfile".to_string(),
            version : "0.1.0".to_string(),
            author : "MAMEDiff".to_string(),
        };

        let mut datafile: Datafile = Datafile {
            header: Some(header),
            machines: vec![],
        };

        let mut keep_machines: HashSet<&String> = HashSet::new();

        keep_machines.extend(&changes.added);
        keep_machines.extend(&changes.modified);

        for (_, new_name) in &changes.renamed {
            keep_machines.insert(new_name);
        }

        for machine in &self.machines {
            if keep_machines.contains(&machine.name) {
                datafile.machines.push(machine.clone());
            }
        }

        datafile
    }
}