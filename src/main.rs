/*
    MAMEDiff (Rust Edition) - A utility & library for comparing MAME datfiles
    Copyright (C) 2021-2024  Thomas Goodman

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use mamediff::compare_files;
use std::path::PathBuf;
use clap::{arg, command, value_parser};

fn main() {

    let mut command = command!()
        .about("A utility for comparing MAME datfiles.")
        .override_usage("mamediff [OPTIONS] (-p OR -x <output file>) <base file> <target file>")
        .arg_required_else_help(true)
        .arg(arg!(<"base file"> "Name of the base XML file")
            .required(true)
            .value_parser(value_parser!(PathBuf)))
        .arg(arg!(<"target file"> "Name of the target XML file")
            .required(true)
            .value_parser(value_parser!(PathBuf)))
        .arg(arg!(-x --xml <"output file"> "Name of the output XML file").id("output file")
            .required(false)
            .value_parser(value_parser!(PathBuf)))
        .arg(arg!(-p --print "Show basic stats about the differences (increase detail with -v)")
            .required(false))
        .arg(arg!(-v --verbose "Show extra details during operation")
            .required(false));

    let help = command.render_help();
    let matches = command.get_matches();

    let base = matches.get_one::<PathBuf>("base file").unwrap().clone();
    let target = matches.get_one::<PathBuf>("target file").unwrap().clone();
    let output = matches.get_one::<PathBuf>("output file").clone();
    let print = matches.get_one::<bool>("print").unwrap().clone();
    let verbose = matches.get_one::<bool>("verbose").unwrap().clone();

    if cli_check(&base, &target, output, print) {
        compare_files(base, target, output, print, verbose);
    } else {
        println!("{}", help);
        std::process::exit(1);
    }
}

fn cli_check(base: &PathBuf, target: &PathBuf, output: Option<&PathBuf>, print: bool) -> bool {
    if output.is_some() || print {
        let base_status: u8 = path_check(base);
        let target_status: u8 = path_check(target);
        let mut output_status: u8 = 0;

        if let Some(output_path) = output {
            output_status = output_check(output_path);
        }

        let file_status: bool = base_status < 1 && target_status < 1 && output_status < 2;

        if file_status {
            return true;
        } else if !file_status && output_status >= 2 {
            println!("Output path is a directory!");
        }
    } else {
        println!("ERROR: No operation (-p or -x) specified!\n");
    }
    false
}

fn path_check(path: &PathBuf) -> u8 {
    if path.exists() && path.is_file() {
        0
    } else if path.exists() && !path.is_file() {
        println!("Not a file: {:#?}", path);
        2
    } else {
        println!("Does not exist: {:#?}", path);
        1
    }
}

fn output_check(path: &PathBuf) -> u8 {
    if path.exists() && path.is_file() {
        0
    } else if path.exists() && !path.is_file() {
        2
    } else {
        1
    }
}