/*
    MAMEDiff (Rust Edition) - A utility & library for comparing MAME datfiles
    Copyright (C) 2021-2024  Thomas Goodman

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use sha2::{Sha256, Digest};

pub use super::Machine;

impl Machine {
    /// Combine hashes for all ROMs present in a machine entry. To keep things deterministic, the
    /// input ROM hashes are always sorted before the hashing operation begins.
    pub fn hash_roms(&mut self) {
        let mut hashed = false;
        let mut hasher = Sha256::new();
        self.roms.sort_by_key(|k| k.sha1);
        for rom in &self.roms {
            if rom.sha1.is_some() {
                hashed = true;
                hasher.update(rom.sha1.unwrap())
            }
        }
        if hashed {
            self.md_rom_hash = Some(<[u8; 32]>::from(hasher.finalize()));
        }
    }

    /// Combine hashes for all disks present in a machine entry. To keep things deterministic, the
    /// input disk hashes are always sorted before the hashing operation begins.
    pub fn hash_disks(&mut self) {
        let mut hashed = false;
        let mut hasher = Sha256::new();
        self.disks.sort_by_key(|k| k.sha1);
        for disk in &self.disks {
            if disk.sha1.is_some() {
                hashed = true;
                hasher.update(disk.sha1.unwrap())
            }
        }
        if hashed {
            self.md_disk_hash = Some(<[u8; 32]>::from(hasher.finalize()));
        }
    }

    /// Combine all rom hashes and disk hashes (if any are present) into an overall machine hash.
    pub fn hash_all(&mut self) {
        self.hash_roms();
        self.hash_disks();

        let mut hasher = Sha256::new();

        if self.md_rom_hash.is_some() || self.md_disk_hash.is_some() {
            if let Some(rom_hash) = self.md_rom_hash {
                hasher.update(rom_hash);
            }
            if let Some(disk_hash) = self.md_disk_hash {
                hasher.update(disk_hash);
            }
            self.md_main_hash = Some(<[u8; 32]>::from(hasher.finalize()));
        }
    }
}