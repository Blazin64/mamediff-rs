/*
    MAMEDiff (Rust Edition) - A utility & library for comparing MAME datfiles
    Copyright (C) 2021-2024  Thomas Goodman

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use serde::{Deserialize, Serialize, Serializer, Deserializer};
use faster_hex::{hex_encode, hex_decode};

pub mod machine_impl;

#[derive(Debug, Serialize, Deserialize, PartialEq, Clone)]
pub struct Machine {
    #[serde(skip_serializing)]
    pub md_main_hash: Option<[u8; 32]>,
    #[serde(skip_serializing)]
    pub md_rom_hash: Option<[u8; 32]>,
    #[serde(skip_serializing)]
    pub md_disk_hash: Option<[u8; 32]>,
    #[serde(rename = "@name")]
    pub name: String,
    #[serde(rename = "@sourcefile", skip_serializing_if = "Option::is_none")]
    pub sourcefile: Option<String>,
    #[serde(rename = "@isbios", skip_serializing_if = "Option::is_none")]
    pub isbios: Option<String>,
    #[serde(rename = "@isdevice", skip_serializing_if = "Option::is_none")]
    pub isdevice: Option<String>,
    #[serde(rename = "@ismechanical", skip_serializing_if = "Option::is_none")]
    pub ismechanical: Option<String>,
    #[serde(rename = "@runnable", skip_serializing_if = "Option::is_none")]
    pub runnable: Option<String>,
    #[serde(rename = "@cloneof", skip_serializing_if = "Option::is_none")]
    pub cloneof: Option<String>,
    #[serde(rename = "@romof", skip_serializing_if = "Option::is_none")]
    pub romof: Option<String>,
    pub description: Option<String>,
    pub year: Option<String>,
    pub manufacturer: Option<String>,
    #[serde(rename = "biosset", default)]
    pub biossets: Vec<BiosSet>,
    #[serde(rename = "rom", default)]
    pub roms: Vec<Rom>,
    #[serde(rename = "disk", default)]
    pub disks: Vec<Disk>,
    #[serde(rename = "device_ref", default)]
    pub devicerefs: Vec<DeviceRef>,
    #[serde(rename = "sample", default)]
    pub samples: Vec<Sample>,
    pub driver: Option<Driver>
}

#[derive(Debug, Serialize, Deserialize, PartialEq, Clone)]
pub struct BiosSet {
    #[serde(rename = "@name")]
    pub name: String,
    #[serde(rename = "@description")]
    pub description: String,
    #[serde(rename = "@default", skip_serializing_if = "Option::is_none")]
    pub default: Option<String>
}

#[derive(Debug, Serialize, Deserialize, PartialEq, Clone)]
pub struct Rom {
    #[serde(skip_serializing)]
    pub md_validated: Option<bool>,
    #[serde(rename = "@name")]
    pub name: String,
    #[serde(rename = "@size")]
    pub size: String,
    #[serde(
        rename = "@crc",
        default,
        serialize_with = "serialize_crc",
        deserialize_with = "deserialize_crc",
        skip_serializing_if = "Option::is_none"
    )]
    pub crc: Option<[u8; 4]>,
    #[serde(
        rename = "@sha1",
        default,
        serialize_with = "serialize_sha1",
        deserialize_with = "deserialize_sha1",
        skip_serializing_if = "Option::is_none"
    )]
    pub sha1: Option<[u8; 20]>,
    #[serde(rename = "@merge", skip_serializing_if = "Option::is_none")]
    pub merge: Option<String>,
    #[serde(rename = "@region", skip_serializing_if = "Option::is_none")]
    pub region: Option<String>,
    #[serde(rename = "@offset", skip_serializing_if = "Option::is_none")]
    pub offset: Option<String>,
    #[serde(rename = "@status", skip_serializing_if = "Option::is_none")]
    pub status: Option<String>,
    #[serde(rename = "@optional", skip_serializing_if = "Option::is_none")]
    pub optional: Option<String>
}

#[derive(Debug, Serialize, Deserialize, PartialEq, Clone)]
pub struct Disk {
    #[serde(rename = "@name")]
    pub name: String,
    #[serde(
        rename = "@sha1",
        default,
        serialize_with = "serialize_sha1",
        deserialize_with = "deserialize_sha1",
        skip_serializing_if = "Option::is_none"
    )]
    pub sha1: Option<[u8; 20]>,
    #[serde(rename = "@merge", skip_serializing_if = "Option::is_none")]
    pub merge: Option<String>,
    #[serde(rename = "@region", skip_serializing_if = "Option::is_none")]
    pub region: Option<String>,
    #[serde(rename = "@index", skip_serializing_if = "Option::is_none")]
    pub index: Option<String>,
    #[serde(rename = "@writable", skip_serializing_if = "Option::is_none")]
    pub writable: Option<String>,
    #[serde(rename = "@status", skip_serializing_if = "Option::is_none")]
    pub status: Option<String>,
    #[serde(rename = "@optional", skip_serializing_if = "Option::is_none")]
    pub optional: Option<String>
}

#[derive(Debug, Serialize, Deserialize, PartialEq, Clone)]
pub struct DeviceRef {
    #[serde(rename = "@name")]
    pub name: String,
}

#[derive(Debug, Serialize, Deserialize, PartialEq, Clone)]
pub struct Sample {
    #[serde(rename = "@name")]
    pub name: String,
}

#[derive(Debug, Serialize, Deserialize, PartialEq, Clone)]
pub struct Driver {
    #[serde(rename = "@status")]
    pub status: String,
    #[serde(rename = "@emulation", skip_serializing_if = "Option::is_none")]
    pub emulation: Option<String>,
    #[serde(rename = "@cocktail", skip_serializing_if = "Option::is_none")]
    pub cocktail: Option<String>,
    #[serde(rename = "@savestate", skip_serializing_if = "Option::is_none")]
    pub savestate: Option<String>,
    #[serde(rename = "@requiresartwork", skip_serializing_if = "Option::is_none")]
    pub requiresartwork: Option<String>,
    #[serde(rename = "@unofficial", skip_serializing_if = "Option::is_none")]
    pub unofficial: Option<String>,
    #[serde(rename = "@nosoundhardware", skip_serializing_if = "Option::is_none")]
    pub nosoundhardware: Option<String>,
    #[serde(rename = "@incomplete", skip_serializing_if = "Option::is_none")]
    pub incomplete: Option<String>
}

fn deserialize_crc<'de, D>(deserializer: D) -> Result<Option<[u8; 4]>, D::Error>
where
    D: Deserializer<'de>,
{
    let hex_option: Option<String> = Option::deserialize(deserializer)?;

    if let Some(hex_string) = hex_option {
        if hex_string.is_empty() {
            return Ok(None);
        }

        if hex_string.len() != 8 {
            return Err(serde::de::Error::custom("Invalid length for CRC"));
        }

        let mut bytes = [0u8; 4];

        match hex_decode(hex_string.as_bytes(), &mut bytes) {
            Ok(_) => Ok(Some(bytes)),
            Err(_) => Err(serde::de::Error::custom("Failed to decode CRC")),
        }
    } else {
        Ok(None)
    }
}

fn serialize_crc<S>(bytes: &Option<[u8; 4]>, serializer: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    if let Some(bytes) = bytes {
        let mut hex_string = [0u8; 8];

        match hex_encode(bytes, &mut hex_string) {
            Ok(hex_str) => serializer.serialize_str(hex_str),
            Err(_) => Err(serde::ser::Error::custom("Failed to encode CRC")),
        }
    } else {
        serializer.serialize_none()
    }
}

fn deserialize_sha1<'de, D>(deserializer: D) -> Result<Option<[u8; 20]>, D::Error>
where
    D: Deserializer<'de>,
{
    let hex_option: Option<String> = Option::deserialize(deserializer)?;

    if let Some(hex_string) = hex_option {
        if hex_string.is_empty() {
            return Ok(None);
        }

        if hex_string.len() != 40 {
            return Err(serde::de::Error::custom("Invalid length for SHA1"));
        }

        let mut bytes = [0u8; 20];

        match hex_decode(hex_string.as_bytes(), &mut bytes) {
            Ok(_) => Ok(Some(bytes)),
            Err(_) => Err(serde::de::Error::custom("Failed to decode SHA1")),
        }
    } else {
        Ok(None)
    }
}

fn serialize_sha1<S>(bytes: &Option<[u8; 20]>, serializer: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    if let Some(bytes) = bytes {
        let mut hex_string = [0u8; 40];

        match hex_encode(bytes, &mut hex_string) {
            Ok(hex_str) => serializer.serialize_str(hex_str),
            Err(_) => Err(serde::ser::Error::custom("Failed to encode SHA1")),
        }
    } else {
        serializer.serialize_none()
    }
}