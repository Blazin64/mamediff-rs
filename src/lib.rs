/*
    MAMEDiff (Rust Edition) - A utility & library for comparing MAME datfiles
    Copyright (C) 2021-2024  Thomas Goodman

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use std::fs::File;
use std::io::{BufReader, BufWriter};
use std::path::PathBuf;
use quick_xml::{Writer, de::from_reader};
use quick_xml::events::{Event, Event::Decl, Event::DocType, BytesDecl, BytesStart, BytesText, };

pub mod datafile;
pub mod machine;

use datafile::Datafile;

/// Find the difference between two MAME XML datafiles and write the difference to a new XML file.
/// Takes 3 file paths (base, target, output) and a boolean verbose flag.
/// Returns nothing. A file is written instead.
pub fn compare_files(base: PathBuf, target: PathBuf, output: Option<&PathBuf>, print: bool, verbose: bool) {
    // Parse base and target files
    if verbose {
        println!("\nReading base file: {:#?}",&base);
        println!("Reading target file: {:#?}\n",&target);
    }

    let mut b_dat: Datafile = Datafile {
        header: None,
        machines: vec![],
    };
    let mut t_dat: Datafile = Datafile {
        header: None,
        machines: vec![],
    };

    match crossbeam::scope(|s| {
        let thread_base = s.spawn(|_| parse_file(&base));
        let thread_target = s.spawn(|_| parse_file(&target));
        b_dat = thread_base.join().unwrap();
        t_dat = thread_target.join().unwrap();
    }) {
        Ok(_) => (),
        Err(e) => {
            eprintln!("Error running threads.");
            eprintln!("  Error: {:?}", e);
            std::process::exit(1);
        }
    }

    if verbose {
        println!("Hashing base machines.\n");
    }

    b_dat.hash_machines();

    if verbose {
        println!("Hashing target machines.\n");
    }

    t_dat.hash_machines();

    if verbose {
        println!("Comparing base and target machines.\n");
    }

    let results = b_dat.compare(&t_dat, &verbose);

    if verbose {
        println!("Generating diff datafile.\n");
    }

    let new_datafile = t_dat.diff_datafile(&results);

    if print {
        // Print some statistics
        println!("Results:");
        if verbose {
            println!("  Base datafile length: {}", &b_dat.machines.len());
            println!("  Target datafile length: {}", &t_dat.machines.len());
            println!("  Diff datafile length: {}", &new_datafile.machines.len());
        }
        println!("  Unchanged: {}", results.unchanged.len());
        println!("  Modified: {}", results.modified.len());
        println!("  Renamed: {}", results.renamed.len());
        println!("  Added: {}", results.added.len());
        println!("  Deleted: {}", results.deleted.len());
    }

    if let Some(output_path) = output {
        create_file(&output_path, &new_datafile, &verbose);
    }
}

/// Parse an XML datafile into a datafile struct.
/// Takes a file path.
/// Returns a datafile struct.
pub fn parse_file(path: &PathBuf) -> Datafile {
    let open_file = File::open(&path);

    let file = match open_file {
        Ok(opened) => opened,
        Err(e) => {
            eprintln!("Unable to open {:?}.",path.display());
            eprintln!("  Error: {:?}", e);
            std::process::exit(1);
        }
    };

    let reader = BufReader::new(file);

    match from_reader(reader) {
        Ok(deserialized) => deserialized,
        Err(e) => {
            eprintln!("Unable to deserialize {:?}.", path.display());
            eprintln!("  Error: {:?}", e);
            std::process::exit(1);
        }
    }
}

// Writes a Datafile object to an XML file
// Takes a path, Datafile object, and a boolean verbose flag.
pub fn create_file(output: &PathBuf, new_datafile: &Datafile, enable_verbose: &bool) {
    let verbose = *enable_verbose;
    // Output file setup
    let o_file = File::create(&output);
    let o_buff = BufWriter::new(o_file.unwrap());
    let mut o_writer = Writer::new_with_indent(o_buff, b'\t', 1);

    // Take care of some XML header stuff before serializing
    o_writer.write_event(Decl(BytesDecl::new("1.0", Some("UTF-8"), None))).unwrap();
    let doctype = BytesText::from_escaped(" datafile PUBLIC \"-//Logiqx//DTD ROM Management Datafile//EN\" \"http://www.logiqx.com/Dats/datafile.dtd\"");
    o_writer.write_event(DocType(doctype)).unwrap();

    // Serialize and write to output file
    if verbose {
        println!("\nWriting output file: {:#?}\n",&output);
    }

    // Set up datafile element start and end
    let start = BytesStart::new("datafile");
    let end = start.to_end();

    // Write datafile start
    o_writer.write_event(Event::Start(start.clone())).unwrap();

    // Write the header, if it exists
    if new_datafile.header.is_some() {
        o_writer.write_serializable("header", &new_datafile.header).unwrap();
    }

    // Write all machine entries
    for machine in &new_datafile.machines {
        o_writer.write_serializable("machine", &machine).unwrap();
    }

    // Write datafile end
    o_writer.write_event(Event::End(end)).unwrap();

}